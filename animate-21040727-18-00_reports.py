# -*- coding: utf-8 -*-
"""
Created on Sat Jul 26 15:45:17 2014
Display a composite radar reflectivity animation, linearly regressing 
the data in file1 and file2. 

USAGE: python animate.py [file1] [file2]
@author: angelo
"""
import datetime as dt
import numpy as np
import matplotlib.pyplot as matplot
from extract import getMessageNo
from radarplot import setRadarLevels, addRadarColorbar
from mpl_toolkits.basemap import Basemap
import matplotlib.animation as animation
import matplotlib.gridspec as gridspec

def linear(t,Ai,Af):
    return Ai + (Af-Ai)*t
    
def ratio2time(t,date1,date2):
    return date1 + dt.timedelta(seconds = (date2-date1).total_seconds()*t)

file1 = './GRIBs/20140727/nam.t18z.conusnest.hiresf00.tm00.refc.grib2'    
file2 = './GRIBs/20140728/nam.t00z.conusnest.hiresf00.tm00.refc.grib2'

refcmsg1 = getMessageNo(file1,1)
refcmsg2 = getMessageNo(file2,1)

projd = refcmsg1.projparams
#projparams are the same for each dataset, they are the grid parameters
latset, lonset = refcmsg2.latlons()
#the data are collected over the same region thus they are equal

#get the data time
date1 = refcmsg1.analDate
date2 = refcmsg2.analDate

#get the reflectivity values
refcmat1 = refcmsg1.values
refcmat2 = refcmsg2.values

print "Computing regression..."
#compute the regressed values over the linspace times
times = np.linspace(0,1,10,endpoint=True)
regrs = [linear(t,refcmat1,refcmat2) for t in times]
isotimes = map(dt.datetime.isoformat,[ratio2time(t,date1,date2) for t in times])

#set backend
matplot.switch_backend('GTK3Agg')
#uncomment next line (and comment previous) if the script is not working
#matplot.switch_backend('Qt4Agg')

print "Creating figure...."
#working on animation
fig = matplot.figure()

#resizing instructions for GTK3 or Qt4 Figure Manager
figman = matplot.get_current_fig_manager()
if matplot.get_backend() == 'GTK3Agg' :
    figman.window.maximize()
elif matplot.get_backend() == 'Qt4Agg' :
    figman.window.showMaximized()

gs = gridspec.GridSpec(20, 1)
gs.update(left=0.05, right=0.95, wspace=0.10, bottom=0.05, hspace = 0.05)
ax1 = matplot.subplot(gs[:-2,:])
cbarax = matplot.subplot(gs[-1,:])
    
m = Basemap(projection=projd['proj'],lat_1=projd['lat_1'],lon_0=projd['lon_0'],\
        lat_0=projd['lat_0'],lat_2=projd['lat_2'],rsphere=(projd['a'],projd['b']),\
        llcrnrlat=latset[0,0],urcrnrlat=latset[-1,-1],\
        llcrnrlon=lonset[0,0],urcrnrlon=lonset[-1,-1],\
        resolution='i',ax=ax1) 
m.drawcoastlines()

#hail report 20140727t2110
lat, lon = 36.76, -83.03
x,y = m(lon,lat)
m.plot(x,y,'o',mec='red',markersize=6,mfc='none',mew=1.2)
#hail report 20140727t2050
lat, lon = 36.91, -84.51
x,y = m(lon,lat)
m.plot(x,y,'o',mec='red',markersize=6,mfc='none',mew=1.2)
#hail report 20140727t2018
lat, lon = 41.28, -85.61
x,y = m(lon,lat)
m.plot(x,y,'o',mec='red',markersize=6,mfc='none',mew=1.2)
#hail report 20140727t2240
lat, lon = 43.65, -72.89
x,y = m(lon,lat)
m.plot(x,y,'o',mec='red',markersize=6,mfc='none',mew=1.2)
#tornado report 20140727t2205
lat, lon = 36.48, -83.86
x,y = m(lon,lat)
m.plot(x,y,'o',mec='blue',markersize=6,mfc='none',mew=1.2)
#hail report 20140727t2153
lat, lon = 36.41, -82.48
x,y = m(lon,lat)
m.plot(x,y,'o',mec='red',markersize=6,mfc='none',mew=1.2)
#hail report 20140727t1900
lat, lon = 43.68, -83.95
x,y = m(lon,lat)
m.plot(x,y,'o',mec='red',markersize=6,mfc='none',mew=1.2)
#hail report 20140727t1830
lat, lon = 43.62, -84.23
x,y = m(lon,lat)
m.plot(x,y,'o',mec='red',markersize=6,mfc='none',mew=1.2)

cmap, cnorm = setRadarLevels()
im = m.imshow(regrs[0],interpolation='nearest',norm=cnorm, cmap=cmap)
addRadarColorbar(im,cbarax)

fig.suptitle('Animation of linear regressed data between ' + \
            isotimes[0] + ' and ' + isotimes[-1], fontsize=16)
       
txt = ax1.set_xlabel(isotimes[0],clip_on=False, va='top')
ax1.xaxis.set_label_position('top') 
      
def refresh(nt):
    global regrs,isotimes,txt,im
    im.set_data(regrs[nt])
    txt.set_text(isotimes[nt])
    
ani = animation.FuncAnimation(fig, refresh, interval=500, frames=len(regrs))

matplot.show()