# -*- coding: utf-8 -*-
"""
Created on Thu Jul 24 18:39:00 2014

Display the data contained in the grib files file1 and file2.

USAGE: python display.py [file1] [file2]

@author: angelo
"""
import matplotlib.pyplot as matplot
from extract import getMessageNo
from radarplot import doubleRadarPlot,radarPlot
import sys

usage = """Display the data contained in the grib files file1 and file2.

USAGE: python display.py [file1] [file2]\n"""

file2 = None
single = False 
    
if len(sys.argv) <= 1:   
    file1 = './demo/20140723/nam.t00z.conusnest.hiresf00.tm00.refc.grib2'
    file2 = './demo/20140723/nam.t06z.conusnest.hiresf00.tm00.refc.grib2' 
    print "Insufficient arguments supplied, running demo mode! \n"
    print usage
elif len(sys.argv) == 2:
    file1 = sys.argv[1]
    if file1.lower() == ('usage' or 'help'):
        print usage
        exit(1)
    single = True
else:
    file1 = sys.argv[1]
    file2 = sys.argv[2]

refcmsg1 = getMessageNo(file1,1)

#get the data time
date1 = refcmsg1.analDate
#get the reflectivity values
refcmat1 = refcmsg1.values

if not file2 == None : 
    refcmsg2 = getMessageNo(file2,1)
    date2 = refcmsg2.analDate
    refcmat2 = refcmsg2.values
        
projd = refcmsg1.projparams
#projparams are the same for each dataset, they are the grid parameters
latset, lonset = refcmsg1.latlons()
#the data are collected over the same region thus they are equal

#set backend
matplot.switch_backend('GTK3Agg')
#uncomment next line (and comment previous) if the script is not working
#matplot.switch_backend('Qt4Agg')

basem = None

if single or file2 == None:
    basem, fig1 = radarPlot(refcmat1,latset,lonset,date1,projd)
else:
    fig1 = doubleRadarPlot([refcmat1, refcmat2],latset,lonset,[date1, date2], projd)
    
fig1.suptitle("Composite reflectivity plot of the dataset(s)",\
                fontsize=12)




matplot.show()    
