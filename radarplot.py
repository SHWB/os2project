# -*- coding: utf-8 -*-
"""
Created on Fri Jul 25 18:36:16 2014

@author: angelo
"""
import numpy as np
import matplotlib.pyplot as matplot
import matplotlib.colors as colors
import matplotlib.gridspec as gridspec
from mpl_toolkits.basemap import Basemap

"""
routine to correctly display the data on the map.
The dipslay logic is:
    colors[i] if thresholds[i] < data < thresholds[i+1] 
This implies len(colors) = len(thresholds - 1) if extend='neither' is specified
Over thresholds[-1] the color is set by set_over(color, alpha)
Under thresholds[0] the color is set by set_under(color, alpha)
"""
def setRadarLevels():
    #set the color map
    #radar reflectivity colors
    cols = ['#00ff00','#00c800','#009000','#ffff00',\
            '#e7c000','#ff9000','#ff8000','#d60000','#c00000','#ff00ff']
    #radar reflectivity levels for PRECIPITATION MODE
    #refcTh = [20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70]    
    #radar reflectivity levels for CLEAN AIR MODE
    refcTh =  [-16, -12, -8, -4, 0, 4, 8, 12, 16, 20, 24] 
    #refcTh = [-20,24] #these exist to show where the data are
    #cols = ['cyan'] #use with above
    cmap, cnorm = colors.from_levels_and_colors(refcTh,cols,extend='neither')
    cmap.set_under(color='w',alpha=0) #below is transparent -> alpha=0
    cmap.set_over(color='#9955c9',alpha=1) #above is magenta
    return (cmap, cnorm)
    
"""
add to the axes cax a colorbar related to the data shown in im
"""    
def addRadarColorbar(im,cbarax):
    matplot.colorbar(im,cax=cbarax,orientation='horizontal',extend='max',\
                    extendfrac='auto',extendrect=True)
    cbarax.set_xlabel('Composite Radar Reflectivity [dB]')
    cbarax.xaxis.set_label_position('top')
    
    
"""
data: numpy 2D array of the data to plot
latitudes: numpy 2D array of latitude data
longitues: numpy 2D array of longitude data
time: datetime object related to the data
projd: proj data object holding geophraphical information of the map
"""
def radarPlot(data, latitudes, longitudes, time, projd):
    #create new figure with incremental number
    fig = matplot.figure()
    
    #resizing instructions for GTK3 or Qt4 Figure Manager
    figman = matplot.get_current_fig_manager()
    if matplot.get_backend() == 'GTK3Agg' :
        figman.window.maximize()
    elif matplot.get_backend() == 'Qt4Agg' :
        figman.window.showMaximized()
    
    gs = gridspec.GridSpec(20, 1)
    gs.update(left=0.05, right=0.95, wspace=0.10, bottom=0.05, hspace = 0.05)
    ax1 = matplot.subplot(gs[:-4,:])     
    cbarax = matplot.subplot(gs[-1,:])
    
    #produce a range of latitude and longitude values spaced by 10 to plot 
    #meridians and circles
    latrange = np.arange(latitudes.min(),latitudes.max(),10)
    lonrange = np.arange(longitudes.min(),longitudes.max(),10)
    #align these values on base ten
    scale = lambda x : int(round(x/10)*10)
    latrange = map(scale, latrange)
    lonrange = map(scale, lonrange)
    
    print "Plotting map..."
        
    m = Basemap(projection=projd['proj'],lat_1=projd['lat_1'],lon_0=projd['lon_0'],\
            lat_0=projd['lat_0'],lat_2=projd['lat_2'],rsphere=(projd['a'],projd['b']),\
            llcrnrlat=latitudes[0,0],urcrnrlat=latitudes[-1,-1],\
            llcrnrlon=longitudes[0,0],urcrnrlon=longitudes[-1,-1],\
            resolution='i',ax=ax1) 
    m.drawcoastlines()
    m.drawparallels(latrange,labels=[1,1,0,1])
    m.drawmeridians(lonrange,labels=[1,1,0,1])
    
    cmap, cnorm = setRadarLevels()
    im = m.imshow(data,interpolation='nearest',norm=cnorm, cmap=cmap)
    ax1.set_xlabel(time, va='top')     
    ax1.xaxis.set_label_position('top')
    addRadarColorbar(im,cbarax)
    
    return m,fig
    
"""
data: 2-element vector. Each element is a numpy 2D array of the data to plot
latitudes: numpy 2D array of latitude data
longitues: numpy 2D array of longitude data
times: 2-element vector of datetime objects related to the data
projd: proj data object holding geophraphical information of the map
"""
def doubleRadarPlot(data,latitudes,longitudes,times, projd):
    #create new figure with incremental number    
    fig = matplot.figure()
    
    #creating the plot layout
    gs = gridspec.GridSpec(20, 2)
    gs.update(left=0.05, right=0.95, wspace=0.10, bottom=0.05)
    ax1 = matplot.subplot(gs[:-2,0])
    ax2 = matplot.subplot(gs[:-2,1])
    cbarax = matplot.subplot(gs[-1,:])
    #placing titles
    ax1.set_title(times[0].isoformat())
    ax2.set_title(times[1].isoformat())
    
    #produce a range of latitude and longitude values spaced by 10 to plot 
    #meridians and circles
    latrange = np.arange(latitudes.min(),latitudes.max(),10)
    lonrange = np.arange(longitudes.min(),longitudes.max(),10)
    #align these values on base ten
    scale = lambda x : int(round(x/10)*10)
    latrange = map(scale, latrange)
    lonrange = map(scale, lonrange)
    
    print "Setting up first map..."
    #draw the map using basemap and the data taken by the grib file
    # lcc is Lambert Conformal Projection
    # llcrnrlat,llcrnrlon,urcrnrlat,urcrnrlon
    # are the lat/lon values of the lower left and upper right corners
    # of the map.
    # resolution i -> intermediate
    
    #placing the first map on the left -> ax1
    m1 =\
    Basemap(projection=projd['proj'],lat_1=projd['lat_1'],lon_0=projd['lon_0'],\
            lat_0=projd['lat_0'],lat_2=projd['lat_2'],rsphere=(projd['a'],projd['b']),\
            llcrnrlat=latitudes[0,0],urcrnrlat=latitudes[-1,-1],\
            llcrnrlon=longitudes[0,0],urcrnrlon=longitudes[-1,-1],\
            resolution='i',ax=ax1)
    m1.drawcoastlines()
    m1.drawparallels(latrange,labels=[1,0,0,1])
    m1.drawmeridians(lonrange,labels=[1,0,0,1])
    
    print "Setting up second map..."
    #placing the second map on the right -> ax2
    m2 =\
    Basemap(projection=projd['proj'],lat_1=projd['lat_1'],lon_0=projd['lon_0'],\
            lat_0=projd['lat_0'],lat_2=projd['lat_2'],rsphere=(projd['a'],projd['b']),\
            llcrnrlat=latitudes[0,0],urcrnrlat=latitudes[-1,-1],\
            llcrnrlon=longitudes[0,0],urcrnrlon=longitudes[-1,-1],\
            resolution='i',ax=ax2)
    m2.drawcoastlines()
    m2.drawparallels(latrange,labels=[0,1,0,1])
    m2.drawmeridians(lonrange,labels=[0,1,0,1])
    
    print "Plotting data"
    cmap, cnorm = setRadarLevels()
    im1 = m1.imshow(data[0],interpolation='nearest',norm=cnorm,cmap=cmap)
    m2.imshow(data[1],interpolation='nearest',norm=cnorm,cmap=cmap)
    
    addRadarColorbar(im1,cbarax)
    ax1.xaxis.set_label_position('top')
    ax2.xaxis.set_label_position('top')
    
    #resizing instructions for GTK3 or Qt4 Figure Manager
    figman = matplot.get_current_fig_manager()
    if matplot.get_backend() == 'GTK3Agg' :
        figman.window.maximize()
    elif matplot.get_backend() == 'Qt4Agg' :
        figman.window.showMaximized()
    
    return fig
    
"""
data: 3-element vector of data to be displayed [left, center, right]
latitudes, longitudes: numpy 2d arrays of geo coordinates
times: 3-element vector holding the datetime(s) of data
projd: projection data
"""    
def triRadarPlot(data, latitudes,longitudes, times, projd):
        
    fig = matplot.figure()    
    
    #creating the plot layout
    gs = gridspec.GridSpec(20, 3)
    gs.update(left=0.05, right=0.95, wspace=0.10, bottom=0.05)
    ax1 = matplot.subplot(gs[:-2,0])
    ax2 = matplot.subplot(gs[:-2,1])
    ax3 = matplot.subplot(gs[:-2,2])
    cbarax = matplot.subplot(gs[-1,:])
    #placing titles
    ax1.set_title(times[0].isoformat())
    ax2.set_title(times[1].isoformat() + " (estimate)")
    ax3.set_title(times[2].isoformat())
    
    #produce a range of latitude and longitude values spaced by 10 to plot 
    #meridians and circles
    latrange = np.arange(latitudes.min(),latitudes.max(),10)
    lonrange = np.arange(longitudes.min(),longitudes.max(),10)
    #align these values on base ten
    scale = lambda x : int(round(x/10)*10)
    latrange = map(scale, latrange)
    lonrange = map(scale, lonrange)   
    
    print "Setting up first map..."

    #placing the first map on the left -> ax1
    m1 =\
    Basemap(projection=projd['proj'],lat_1=projd['lat_1'],lon_0=projd['lon_0'],\
            lat_0=projd['lat_0'],lat_2=projd['lat_2'],rsphere=(projd['a'],projd['b']),\
            llcrnrlat=latitudes[0,0],urcrnrlat=latitudes[-1,-1],\
            llcrnrlon=longitudes[0,0],urcrnrlon=longitudes[-1,-1],\
            resolution='i',ax=ax1)
    m1.drawcoastlines()
    m1.drawparallels(latrange,labels=[1,0,0,1])
    m1.drawmeridians(lonrange,labels=[1,0,0,1])
    
    print "Setting up second map..."
    #placing the second map on center -> ax2
    m2 =\
    Basemap(projection=projd['proj'],lat_1=projd['lat_1'],lon_0=projd['lon_0'],\
            lat_0=projd['lat_0'],lat_2=projd['lat_2'],rsphere=(projd['a'],projd['b']),\
            llcrnrlat=latitudes[0,0],urcrnrlat=latitudes[-1,-1],\
            llcrnrlon=longitudes[0,0],urcrnrlon=longitudes[-1,-1],\
            resolution='i',ax=ax2)
    m2.drawcoastlines()
    m2.drawparallels(latrange,labels=[0,0,0,1])
    m2.drawmeridians(lonrange,labels=[0,0,0,1])
    
    print "Setting up third map..."
    #placing the second map on the right -> ax3
    m3 =\
    Basemap(projection=projd['proj'],lat_1=projd['lat_1'],lon_0=projd['lon_0'],\
            lat_0=projd['lat_0'],lat_2=projd['lat_2'],rsphere=(projd['a'],projd['b']),\
            llcrnrlat=latitudes[0,0],urcrnrlat=latitudes[-1,-1],\
            llcrnrlon=longitudes[0,0],urcrnrlon=longitudes[-1,-1],\
            resolution='i',ax=ax3)
    m3.drawcoastlines()
    m3.drawparallels(latrange,labels=[0,1,0,1])
    m3.drawmeridians(lonrange,labels=[0,1,0,1])
    
    print "Plotting data"
    cmap, cnorm = setRadarLevels()
    im1 = m1.imshow(data[0],interpolation='nearest',norm=cnorm,cmap=cmap)
    m2.imshow(data[1],interpolation='nearest',norm=cnorm,cmap=cmap)
    m3.imshow(data[2],interpolation='nearest',norm=cnorm,cmap=cmap) 
    
    addRadarColorbar(im1,cbarax)
    ax1.xaxis.set_label_position('top')
    ax2.xaxis.set_label_position('top')
    ax3.xaxis.set_label_position('top')
    
    #resizing instructions for GTK3 or Qt4 Figure Manager
    figman = matplot.get_current_fig_manager()
    if matplot.get_backend() == 'GTK3Agg' :
        figman.window.maximize()
    elif matplot.get_backend() == 'Qt4Agg' :
        figman.window.showMaximized()
        
    return fig    
