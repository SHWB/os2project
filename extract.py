# -*- coding: utf-8 -*-
"""
Created on Thu Jul 24 16:46:42 2014

This script is used to extract a particular message from multiple grib files, 
searching by short name. 

Example:
    -(extract reflectivity composite)
    python extract.py refc [file.grib] [file2.grib]   
    
@author: Angelo Barboni
"""

import pygrib
import sys


usage="""Extract from grib a message given its shortname. Supports multiple files.

USAGE: python extract.py [shortname] [file1] [file2] ...

EXAMPLE: python extract.py refc 20140722.0000.measures.grib2 20130722.0600.measures.grib2
    """    
"""
returns a list of matches in a tuple given a key, plus their message number.
Tip: use the returned message number to extract the message with getMessageNo
"""
def tupleSearch(tup,key):
    out = []
    for i,attribute in enumerate(tup):
        if key in attribute:
            out.append([i,attribute])
    return out
    
"""
getMessageIn(sourcegrib, attribute,value)
sourcegrib: the name or path of the grib file to extract the message from
attribute: the attribute name to search
key: the attribute value to filter

i.e.: getMessageIn(gribfile,'shortName','refc') will return the first message 
"""
def getMessageBy(sourcegrib, attribute, value):
    msg = None
    try:
        print "Opening grib file: " + sourcegrib
        gribf = pygrib.open(sourcegrib)
        print "File loaded, searching for: " + attribute +" = "+ value
        try:
            gribf.seek(0)
            msg = gribf.select(**{attribute:value})[0]
        except IOError:
            print "IoError, couldn't provide message"
            exit(1)
        except ValueError:
            print "ValueError: 404 keyword not found: "+ value
            print usage
            exit(1)            
        except Exception:
            raise
        finally:
            gribf.close()
    except Exception as e:
        print "File open error: check the path or file type" 
        raise e
    return msg

"""
getMessageNo(sourcegrib, messageno)
sourcegrib: the name or path of the grib file to extract the message from
messageno: the message shortname
"""
def getMessageNo(sourcegrib, messageno):
    msg = None
    try:
        print "Opening grib file: " + sourcegrib
        gribf = pygrib.open(sourcegrib)
        try:
            msg = gribf.message(messageno)
            print "Loaded"
        except IOError:
            print "IoError, couldn't provide message"
            exit(1)
        except ValueError:
            print "ValueError: 404 message not found"
            exit(1)
        except Exception:
            raise
        finally:
            gribf.close()
    except Exception:
        print "File open error: check the path, filename or type"
        raise 
    return msg

"""
saveMsgAsGrib(msg, dest)
msg: the message as gribmessage to be saved as a standalone grib
dest: the destination file name or path
"""    
def saveMsgAsGrib(msg, dest):
    print "Saving as GRIB...: "+ dest
    binmsg = msg.tostring()
    try:
        grbout = open(dest,'wb')
        try:
            grbout.write(binmsg)
            print "Done."
        except Exception as e:
            print "Couldn't write file."
            print str(e.message)
        finally:
            grbout.close()
    except IOError:
        print "File open error..."
        raise
        
"""
return a tuple of all the message keys
"""
def getMessageKeys(msg):
    return tuple(map(str,msg.keys()))

"""
return a tuple of message keys even partially matching the given one
"""   
def getMatchingKey(msg,key):
    keys = getMessageKeys(msg)
    tupleSearch(keys,key)

"""
for each message in the src grib, return the value in the key attribute
"""    
def getAllbyKey(src,key):
    gribs = None
    print "Fetching all " + key + " attributes in file " + src
    try:   
        gribs = pygrib.open(src)
    except Exception:
        print "File open error: check the path, filename or type. Returning None"
    return tuple(map(str,[grb[key] for grb in gribs])) if not gribs == None else None
            
#main 
if __name__ == '__main__':
    if len(sys.argv) < 3:
        print "Insufficient arguments provided."
        print usage
    else:
        key = sys.argv[1].lower()
        for i in range(2,len(sys.argv)):
            src = sys.argv[i] 
            amsg = getMessageBy(src,'shortName',key)
            outgrib = src[0:-6] + "." + key + ".grib2"
            saveMsgAsGrib(amsg,outgrib)   
