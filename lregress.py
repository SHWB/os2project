# -*- coding: utf-8 -*-
"""
Created on Sat Jul 26 17:13:22 2014

Perform linear regression on the data in file1 and file2, displaying
them as well as the data estimated in t. The parameter t is the time ratio in 
the interval between file1 and file2 analysis times.

USAGE:
    python lregress.py file1 file2 [float in (0,1)]
    
@author: angelo
"""
import datetime as dt
import matplotlib.pyplot as matplot
from extract import getMessageNo, saveMsgAsGrib
from radarplot import triRadarPlot
import sys

def linear(t,Ai,Af):
    return Ai + (Af-Ai)*t
    
def ratio2time(t,date1,date2):
    return date1 + dt.timedelta(seconds = (date2-date1).total_seconds()*t)


usage = """Perform linear regression on the data in file1 and file2, displaying
them as well as the data estimated in t. The parameter t is the time ratio in 
the interval between file1 and file2 analysis times.

USAGE: python lregress.py [file1] [file2] [float in (0,1)]\n"""
t = 0.5
file1 = './demo/20140723/nam.t00z.conusnest.hiresf00.tm00.refc.grib2'
file2 = './demo/20140723/nam.t06z.conusnest.hiresf00.tm00.refc.grib2' 
  
if len(sys.argv) <= 3:
    print "Insufficient arguments supplied, running demo mode!\n"
    print usage
else: 
    file1 = sys.argv[1]
    file2 = sys.argv[2]
    try:
        tmp = float(sys.argv[3])
        if not tmp.is_integer() and 0 < tmp < 1:
            t = tmp
        else:
            print "Invalid time interval ratio: using t = " + str(t)
            print usage
    except ValueError as er:
        print er.message
        print "Invalid time parameter, using t = " + str(t)
        print usage
       
refcmsg1 = getMessageNo(file1,1)
refcmsg2 = getMessageNo(file2,1)

projd = refcmsg1.projparams
#projparams are the same for each dataset, they are the grid parameters
latset, lonset = refcmsg2.latlons()
#the data are collected over the same region thus they are equal

#get the data time
date1 = refcmsg1.analDate
date2 = refcmsg2.analDate

#get the reflectivity values
refcmat1 = refcmsg1.values
refcmat2 = refcmsg2.values

#set backend
matplot.switch_backend('GTK3Agg')
#uncomment next line (and comment previous) if the script is not working
#matplot.switch_backend('Qt4Agg')

regrtime =  ratio2time(t,date1,date2)
print "Regressing for " + regrtime.isoformat() + "..."
regr = linear(t,refcmat1,refcmat2)

"""
fig2 = triRadarPlot([refcmat1,regr,refcmat2],latset,lonset,\
                    [date1,regrtime,date2],projd)
fig2.suptitle("Composite reflectivity plot at three different times", \
                fontsize=12)

#display every figure created
matplot.show()
"""
#script section that saves the interpolated data

#building the new file
outname = "./GRIBs/regressed/lreg."+regrtime.date().isoformat()+".t"+\
            str(regrtime.hour).zfill(2)+"z.conusnest.hiresf00.tm"+\
            str(regrtime.minute).zfill(2)+".refc.grib2"

#create a copy, refcmsg1 may be needed again somehow            
refctemp = refcmsg1
#setting grib attributes to the interpolated data
#suppose all inherited except values and datetime
refctemp.values = regr
refctemp.hour = regrtime.hour
refctemp.minute = regrtime.minute
refctemp.second = regrtime.second
refctemp.day = regrtime.day
refctemp.month = regrtime.month
refctemp.year = regrtime.year

saveMsgAsGrib(refctemp,outname)
            