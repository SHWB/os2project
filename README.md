README
======
Contents and basic usage
--------

* `extract.py` is both a script used to extract a particular message by short name from a list of files. It also includes several utility function to parse, inventory and write GRIB files. Example: how to extract composite reflectivity.

        python extract.py refc [file1] [file2] ....[filen]  
      

* `radarplot.py` is an essential mini-library containing some functions to plot (or multi-plot) composite reflectivity data on a map. 


* `display.py` is a script used to display grib files containing reflectivity data previously extracted by `extract.py`. It's possible to display one or two plots side by side depending on the arguments number. Examples of use are:

        python display.py [file1]
        python display.pu [file1] [file2]  


* `lregress.py` is a script that performs linear regression on two datasets, for a given normalized time ratio t. Thus, t must be a float in [0,1]. It also saves the result as a GRIB. Valid syntax therefore is:

        python lregress.py [file1] [file2] [t]  


* `animate.py` is a script that interpolates data over a ten-steps interval between `file1` and `file2`, showing the regression and the evolution by animation. Usage:

        python animate.py [file1] [file2]  


* `animate-xxxxxx-yy-zz_reports.py` are premade demo scripts which show an animation of the data in the day `xxxxxx` between hours `yy` and `zz`. They include a handful of reports taken from the NOAA's Weather Service.


Dependencies  
------------


    -+python 2.7
     |  (modules)
     +--+ pygrib 1.9.8 
     |  |  
     |  +--+ GRIB API C library 1.8.0 (or higher) 
     |  |  + build tools (gcc, g++)
     |  |  + libpng (usually a matplotlib dep)
     |  |  + libjasper or openjpeg 
     |  |
     |  + pyproj 
     |  + numpy 1.2.1 (or higher)
     |  + matplotlib (with dependencies)
     |  + basemap    
     |
    -+ GTK or Qt4 support

Notes
-----

GRIB API C can be downloaded at the given link and built following the installation instructions there.
matplotlib requires several dependencies: it's possible to resolve them easily using `'sudo apt-get build-dep matplotlib'`, if you're working in a debian OS.
The matplotlib and pyproj can be installed through the resident OS package manager, or python-pip.
Note that pip doesn't automatically resolve dependencies (i.e. pyproj will complain if libgeos is not installed).
Also note that pip may need `"--allow-unverified [package]"` option.
Installation through the OS package manager is adviced, when possible.

When installing pygrib, remember to edit the setup.cfg.template and save it as setup.cfg. You can find the info it requires using the unix command "find /usr -name <lib>" (the required libraries are usually in the /usr tree). Note the names and fill the setup.cfg. You can either decide to export the variables listed in the pygrib documentation.
When installing pygrib, remember to edit the setup.cfg.template and save it as setup.cfg. You can find the info it requires using the unix command "find /usr -name <lib>" (the required libraries are usually in the /usr tree). Note the names and fill the setup.cfg. You can either decide to export the variables listed in the pygrib documentation.